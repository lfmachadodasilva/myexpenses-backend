﻿namespace lfmachadodasilva.MyExpenses.Api.Models.Dtos
{
    public class UserDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
