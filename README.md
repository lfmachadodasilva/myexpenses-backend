# MyExpenses 

[![Build Status](https://travis-ci.com/lfmachadodasilva/myexpenses-backend.svg?branch=master)](https://travis-ci.com/lfmachadodasilva/myexpenses-backend) 
[![Build status](https://ci.appveyor.com/api/projects/status/qelwk218xrn3m3ni/branch/master?svg=true)](https://ci.appveyor.com/project/lfmachadodasilva/myexpenses-backend/branch/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/06a721b99cc043978c117751139d1cf6)](https://www.codacy.com/app/lfmachadodasilva/myexpenses-backend?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=lfmachadodasilva/myexpenses-backend&amp;utm_campaign=Badge_Grade)
[![codecov](https://codecov.io/gh/lfmachadodasilva/myexpenses-backend/branch/master/graph/badge.svg)](https://codecov.io/gh/lfmachadodasilva/myexpenses-backend)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=myexpenses-backend&metric=alert_status)](https://sonarcloud.io/dashboard?id=myexpenses-backend)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=myexpenses-backend&metric=coverage)](https://sonarcloud.io/dashboard?id=myexpenses-backend)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=myexpenses-backend&metric=ncloc)](https://sonarcloud.io/dashboard?id=myexpenses-backend)
